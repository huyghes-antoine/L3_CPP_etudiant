#include "Livre.hpp"
#include <iostream>

Livre::Livre(): _titre(""), _auteur(""), _annee(0) {}

Livre::Livre(const std::string & titre, const std::string & auteur, int annee): _titre(titre), _auteur(auteur), _annee(annee){


    if(titre.find(";") != std::string::npos or titre.find("\n") != std::string::npos){
       throw (std::string ("Erreur titre"));
    }
    if(auteur.find(";") != std::string::npos or auteur.find("\n") != std::string::npos){
           throw (std::string ("Erreur auteur"));
        }
}
const std::string & Livre::getTitre() const{
    return _titre;

}
const std::string & Livre::getAuteur() const{
    return _auteur;
}
int Livre::getAnnee() const {
    return _annee;
}

bool Livre::operator<(const Livre &a){
    if(_auteur < a.getAuteur())
        return true;
    else if(_auteur == a.getAuteur() && _titre < a.getTitre() )
        return true;
    else
        return false;
}

bool Livre::operator==(const Livre &a){
    return (_auteur == a.getAuteur() && _titre == a.getTitre() && _annee == a.getAnnee());
}

std::ostream& operator<<(std::ostream &flux, Livre const& livre){
        flux << livre.getTitre()<< ";" << livre.getAuteur() << ";" << livre.getAnnee();
        return flux;
}
std::istream& operator>>(std::istream& is, Livre& livre){
    std::string annee;
    std::getline(is, livre._titre, ';');
    std::getline(is, livre._auteur,';');
    std::getline(is, annee, ';');
    livre._annee = std::stoi(annee);
    return is;
}
