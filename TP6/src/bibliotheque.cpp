#include "Bibliotheque.hpp"
#include <iostream>
#include <algorithm>
#include <fstream>

void Bibliotheque::afficher() const{
    for(const Livre &l: *this){
        std::cout << l << std::endl;
    }
}

void Bibliotheque::trierParAuteurEtTitre(){
    sort(begin(), end());
}

void Bibliotheque::trierParAnnee(){
    auto fAnnee = [](Livre l1, Livre l2){
        return l1.getAnnee() < l2.getAnnee();};
    std::sort(this->begin(), this->end(), fAnnee);
}

void Bibliotheque::lireFichier(const std::string &nomFichier){

}

void Bibliotheque::ecrireFichier(const std::string &nomFichier) const{
    std::ofstream ofs(nomFichier);
    for(const Livre & l : *this)
            ofs << l << std::endl;
}
