#include "couleur.hpp"
#include "figuregeometrique.hpp"
#include "ligne.hpp"
#include "point.hpp"
#include "polygoneregulier.hpp"
#include <vector>

#include <iostream>

using namespace std;

int main() {
/*
    FigureGeometrique f(Couleur{1.0,2.0,3.0});
    Couleur c = f.getCouleur();
    Ligne l(c, {0,0}, {100, 200});
    l.afficher();
    PolygoneRegulier polygone({0,1,0}, {100,200}, 50, 5);
    polygone.afficher();
*/
    vector <FigureGeometrique*> figures{
        new Ligne({1,0,0}, {0,0}, {100,200}),
        new PolygoneRegulier({0,1,0}, {100,200}, 50, 5)
    };

    for(FigureGeometrique * f : figures)
        f->afficher();

    for(FigureGeometrique * f : figures)
        delete f;

    return 0;
}

