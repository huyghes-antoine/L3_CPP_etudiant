#include "ligne.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupLigne){ };

TEST(GroupLigne, test1){
    Couleur rouge {1, 0, 0};
    Ligne l (rouge, {1,2}, {3,4});

    Couleur c = l.getCouleur();
    Point p0 = l.getP0();
    Point p1 = l.getP1();

    CHECK_EQUAL(c._r, 1.0);
    CHECK_EQUAL(c._g, 0.0);
    CHECK_EQUAL(c._b, 0.0);

    CHECK_EQUAL(p0._x, 1);
    CHECK_EQUAL(p0._y, 2);

    CHECK_EQUAL(p1._x, 3);
    CHECK_EQUAL(p1._y, 4);
}
