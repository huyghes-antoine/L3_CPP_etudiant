#define _USE_MATH_DEFINES
#include "polygoneregulier.hpp"
#include <cmath>


PolygoneRegulier::PolygoneRegulier(const Couleur & couleur, const Point & center, int rayon, int nbCotes):
    FigureGeometrique(couleur), _nbPoints(nbCotes)
{
   _points = new Point[_nbPoints];
    for(int i=0; i<_nbPoints; i++){
        double angle = 2*M_PI*i/double(_nbPoints);
        int x = rayon * cos(angle) + center._x;
        int y = rayon * sin(angle) + center._y;
        _points[i] = {x,y};
    }
}

void PolygoneRegulier::afficher() const{
    const Couleur & c = getCouleur();
    cout << "PolygoneRegulier "
         << c._r << "_" << c._g << "_" << c._b;

    for(int i=0; i<_nbPoints; i++){
        const Point & p = _points[i];
        cout << " " << p._x << "_" << p._y;
    }
    cout << endl;
}

int PolygoneRegulier::getNbPoints() const{
    return _nbPoints;
}

const Point & PolygoneRegulier::getPoint(int indice) const{
    return _points[indice];
}


PolygoneRegulier::~PolygoneRegulier(){
 delete [] _points;
}
