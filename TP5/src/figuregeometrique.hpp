#ifndef FIGUREGEOMETRIQUE_HPP
#define FIGUREGEOMETRIQUE_HPP
#include <iostream>
#include "couleur.hpp"


class FigureGeometrique
{
protected:
    Couleur _couleur;
public:
    FigureGeometrique(const Couleur & couleur);
    const Couleur & getCouleur() const;
    virtual void afficher() const = 0;
    virtual ~FigureGeometrique() = default;
};

#endif // FIGUREGEOMETRIQUE_HPP
