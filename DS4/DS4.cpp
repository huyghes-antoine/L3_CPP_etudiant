#include<iostream>
#include<string>

class Personne {
		protected:
			std::string _nom;
		public: 
			Personne(const std::string & nom) : _nom(nom){}
			virtual void afficher() const = 0;
			};
			

class Etudiant : public Personne {
	private: 
		float _moyenne;
	public: 
		Etudiant(const std::string &, float moyenne) : Personne(_nom), _moyenne(moyenne){}
		void afficher() const override {  // On override la méthode de la classe fille ( override permet d'afficher les erreurs)
				std::cout << _nom << '; '<< _moyenne << std::endl;}
	};
	
class Prof: public Personne {
		private:
			double _salaire; 
		public: 
			Prof(const std::string &, double salaire) : Personne(_nom), _salaire(salaire){}
			void afficher() const override{
					std::cout << _nom << ';' << _salaire << std::endl;}
}
	
int main(){
	Personne p1("toto");
	p1.afficher();
	Etudiant e1("tutu", 42.f);
	e1.afficherEtudiant();
	return 0
}


