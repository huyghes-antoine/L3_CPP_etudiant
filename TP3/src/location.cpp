#include "location.hpp"
#include <iostream>

using namespace std;

void Location::afficherLocation() const{
    cout << "Location (" << _idClient << ", "<< _idProduit <<")" << endl;
}

Location::Location(int idClient, int idProduit){
 _idClient = idClient;
 _idProduit = idProduit;
}
