#include "client.hpp"
#include <iostream>
#include <string>

using namespace std;

client::client(int id,const string & nom)
{
    _id = id;
    _nom = nom;
}

int client::getId() const{
    return _id;
}

const string & client::getNom() const{
    return _nom;
}

void client::afficherClient() const {
    cout << "Client (" << getId() << ", "<< getNom() <<")" << endl;
}
