#ifndef PRODUIT_HPP
#define PRODUIT_HPP
#include <iostream>
#include <string>

using namespace std;

class produit
{
private:
        int _id;
        string _description;
public:
    produit(int id, const string & description);
    int getId() const;
    const string & getDescription() const;
    void afficherProduit() const;
};

#endif // PRODUIT_HPP
