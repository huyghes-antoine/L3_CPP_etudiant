#ifndef CLIENT_HPP
#define CLIENT_HPP
#include <string>
#include <iostream>

using namespace std;

class client
{
private:
    int _id;
    string _nom;
public:
    client(int id, const string & nom);
    int getId() const;
    const string & getNom() const;
    void afficherClient() const;
};

#endif // CLIENT_HPP
