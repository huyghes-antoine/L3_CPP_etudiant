#include "magasin.hpp"

#include <iostream>

using namespace std;

magasin::magasin(): _idCourantClient(0), _idCourantProduit(0)
{

}

int magasin::nbClients() const{
    return _clients.size();
}

void magasin::ajouterClient(const string & nom){
    _clients.push_back(client(_idCourantClient, nom));
    _idCourantClient ++;
}

void magasin::afficherClients() const{
    for(int i=0; i<nbClients() ;i++)
        _clients[i].afficherClient();
}

void magasin::supprimerClient(int idClient){
    for(int i=0; i<nbClients(); i++){
        if(_clients[i].getId() == idClient){
        swap(_clients[i], _clients[nbClients()-1]);
        _clients.pop_back();
        return;
        }
    }
    throw string ("erreur : ce client n’existe pas");
}


int magasin::nbProduits() const{
    return _produits.size();
}

void magasin::ajouterProduit(const string & nom){
    _produits.push_back(produit(_idCourantProduit, nom));
    _idCourantProduit ++;
}

void magasin::afficherProduits() const{
    for(int i=0; i<nbProduits(); i++)
        _produits[i].afficherProduit();
}

void magasin::supprimerProduit(int idProduit){
    for(int i=0; i<nbProduits(); i++){
        if(_produits[i].getId() == idProduit){
            swap(_produits[i], _produits[nbProduits()-1]);
            _produits.pop_back();
            return;
        }
    }
    throw string ("erreur : ce produit n’existe pas");
}

int magasin::nbLocations() const{
    return _locations.size();
}

void magasin::ajouterLocation(int idClient, int idProduit){
    _locations.push_back(Location(idClient,idProduit));
}

void magasin::afficherLocation() const{
    for(int i =0; i<nbLocations(); i++)
            _locations[i].afficherLocation();
}

/*void magasin::supprimerLocation(int idClient, int idProduit){
   for(int i = 0; i<nbLocations(); i++){
        if(_locations[i].)
    }
}*/
