#include "client.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupClient){};
TEST(GroupClient, Client_test){
    client c(21, "tonio");
    CHECK_EQUAL(c.getId(), 21);
    CHECK_EQUAL(c.getNom(), "tonio");
}
