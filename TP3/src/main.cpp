#include <iostream>
#include <string>
#include "location.hpp"
#include "client.hpp"
#include "magasin.hpp"
#include "produit.hpp"

int main(){
Location l(0,2);
l.afficherLocation();

client c(42, "toto");
c.afficherClient();

produit p(10, "oui non oui");
p.afficherProduit();

magasin m;

m.ajouterClient("Pierre");
m.ajouterClient("Théo");
m.ajouterClient("Vincent");
m.afficherClients();

try{
m.supprimerClient(4);
m.afficherClients();
}
catch(string e){
    cout << e << endl;
}

m.ajouterProduit("coton tige");
m.ajouterProduit("album de patrick sebastien");
m.afficherProduits();

try{
    m.supprimerProduit(3);
    m.afficherProduits();
}
catch(string e){
    cout << e << endl;
}


m.ajouterLocation(1, 1);
m.ajouterLocation(0,1);
m.afficherLocation();
cout << m.nbLocations() << endl;
}
