#include "magasin.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupMagasin){};
TEST(GroupMagasin, Magasin_test){
    magasin m;
    m.ajouterClient("moi");
    m.ajouterClient("toi");
    try{
        m.supprimerClient(3);
        CHECK(false);
    }
    catch(const string &e){
        CHECK(true);
    }

}

TEST(GroupMagasin, Magasin_test2){
     magasin m;
     m.ajouterProduit("coton tige");
     m.ajouterProduit("Album de patrick sebastien");
     try{
         m.supprimerProduit(3);
         CHECK(false);
     }
     catch(const string &e){
         CHECK(true);
     }
}
