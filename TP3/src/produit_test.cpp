#include "produit.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupProduit){};
TEST(GroupProduit, Produit_test){
    produit p(10, "oui non oui");
    CHECK_EQUAL(p.getId(), 10);
    CHECK_EQUAL(p.getDescription(), "oui non oui");
}
