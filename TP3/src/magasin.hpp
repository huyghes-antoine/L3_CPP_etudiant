#ifndef MAGASIN_HPP
#define MAGASIN_HPP
#include "location.hpp"
#include "produit.hpp"
#include "client.hpp"
#include <vector>
#include <iostream>

using namespace std;


class magasin
{
private:

vector<client> _clients;
vector <produit> _produits;
vector <Location> _locations;
int _idCourantClient;
int _idCourantProduit;

public:
    magasin();
    int nbClients() const;
    void ajouterClient(const string & nom);
    void afficherClients() const;
    void supprimerClient(int idClient);

    int nbProduits() const;
    void ajouterProduit(const string & nom);
    void afficherProduits() const;
    void supprimerProduit(int idProduit);

    int nbLocations() const;
    void ajouterLocation(int idClient, int idProduit);
    void afficherLocation() const;
   // void supprimerLocation(int idClient, int idProduit);
   // bool trouverClientDansLocation(int idClient) const;
   // vector<int> calculerClientsLibres() const;
   // bool trouverProduitDansLocation(int idProduit) const;
   // vector<int> calculerProduitsLibres() const;
};

#endif // MAGASIN_HPP
