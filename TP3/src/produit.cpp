#include "produit.hpp"
#include <iostream>
#include <string>

using namespace std;

produit::produit(int id, const string & description)
{
    _id = id;
    _description = description;
}

int produit::getId() const{
    return _id;
}

const string & produit::getDescription() const{
    return _description;
}

void produit::afficherProduit() const {
    cout << "Produit (" << getId() << ", "<< getDescription() <<")" << endl;
}
