#ifndef IMAGE_HPP
#define IMAGE_HPP
#include <iostream>
#include <vector>
#include <string>


class Image
{
private:
    int _largeur;
    int _hauteur;
    int * _pixels;
public:
    Image(int largeur, int hauteur);
    Image(const Image & img);
    ~Image();

    const Image & operator = (const Image &img);

    int getLargeur() const;
    int getHauteur() const;

    // Version de base
    int getPixel(int i, int j) const;
    void setPixel(int i, int j, int couleur);

    // Versions par réferences
    int pixel(int i, int j) const;
    int & pixel(int i, int j);
};

void ecrirePnm(const Image &img, const std::string & nomFichier);
void remplir(Image &img);
Image bordure(const Image &img, int couleur);

#endif // IMAGE_HPP
