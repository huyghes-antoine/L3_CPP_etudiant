cmake_minimum_required( VERSION 3.0 )
project( MON_PROJET )

add_executable( Main
    Main.cpp Fibonacci.cpp )

# dépendances
find_package( PkgConfig REQUIRED )
pkg_check_modules( PKG_CPPUTEST REQUIRED cpputest )
include_directories( ${PKG_CPPUTEST_INCLUDE_DIRS} )


add_executable( main_test
    main_test.cpp  Fibonacci.cpp fibonacci_test.cpp )
target_link_libraries( main_test
    ${PKG_CPPUTEST_LIBRARIES} )
