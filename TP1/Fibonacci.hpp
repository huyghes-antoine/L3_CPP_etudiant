#ifndef FIBONACCI_HPP
#define FIBONACCI_HPP

#include <iostream> 

int fibonacciRecursif(int n);
int fibonacciIteratif(int n);

#endif 

