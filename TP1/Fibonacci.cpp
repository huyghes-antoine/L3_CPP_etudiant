#include <iostream> 
#include "Fibonacci.hpp"
using namespace std;
 
int fibonacciRecursif(int n){
	if(n==0)
		return 0; 
	if(n==1)
		return 1; 
	else 
		return fibonacciRecursif(n-1) + fibonacciRecursif(n-2); 
}
	 
	 
int fibonacciIteratif(int n){
		int premier = 0;
		int second = 1; 
		int tmp;
		
		for(int i = 0; i < n; i++){
			tmp = premier + second; 
			premier  = second; 
			second = tmp;
		}
	return premier;
}


