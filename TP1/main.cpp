// on va dans le build, on fait cmake .. pour cmake le dossier contenant les codes 
// make pour compiler grâce au CMakeLists.txt 
// puis on lance le fichier "Main" selon le nom donné dans le CMakeLists.txt 



#include <iostream>
#include "Fibonacci.hpp"

int main(){
		std::cout << fibonacciIteratif(7) << std::endl <<  fibonacciRecursif(7) << std::endl;
		return 0;
}


