#include "viewerfigures.hpp"


viewerFigures::viewerFigures(int argc, char ** argv):
    _kit(argc, argv)
{
    _window.set_title("Dessin en HD");
    _window.set_default_size(640, 480);
    _window.add(_dessin);

       _window.add_events(Gdk::BUTTON_PRESS_MASK);
        _window.signal_button_press_event().connect(sigc::mem_fun(&_dessin, &zoneDessin::gererClic));
    _window.show_all();
}

void viewerFigures::run(){
    _kit.run(_window);
}
