#include "zonedessin.hpp"
#include"polygoneregulier.hpp"
#include"ligne.hpp"


zoneDessin::zoneDessin():
    _engine(std::random_device{}()), _distRayon(20 ,80), _distCotes(3,8){
 _figures.push_back(new Ligne({1,0,0},{10,10},{630,10}));
   _figures.push_back(new Ligne({1,0,0},{630,10},{630,470}));
    _figures.push_back(new Ligne({1,0,0},{10,470},{630,470}));
     _figures.push_back(new Ligne({1,0,0},{10,10},{10,470}));
_figures.push_back(new PolygoneRegulier({0,1,0},{100,200}, 50,5));
}

zoneDessin::~zoneDessin(){
    for(auto x: _figures){
        delete x;
        x=nullptr;
    }
}

bool zoneDessin::on_draw(
        const Cairo::RefPtr<Cairo::Context> &context){


    for(FigureGeometrique * figures : _figures)
           figures->afficher(context);


    return true;

}

bool zoneDessin::gererClic(GdkEventButton* event){

    if (event->button == 1){
        int rayon = _distRayon(_engine);
        int cotes = _distCotes(_engine);

        _figures.push_back(new PolygoneRegulier({0,1,0},{event->x, event->y}, rayon, cotes));
    }
    else if(event -> button == 3){
        if(not _figures.empty()){
            _figures.pop_back();
        }
    }


    auto win = get_window();
    win->invalidate(true);

    return true;
}
