#include "couleur.hpp"
#include "figuregeometrique.hpp"
#include "ligne.hpp"
#include "point.hpp"
#include "polygoneregulier.hpp"
#include <vector>
#include <gtkmm.h>
#include"viewerfigures.hpp"
#include <iostream>

using namespace std;

int main(int argc, char ** argv) {

   /* Gtk::Main kit(argc, argv); // application gtkmm
        Gtk::Window window; // fenetre principale
        Gtk::Label label(" Hello world ! "); // message
        window.add(label);
        window.show_all();
        kit.run(window); // lance la boucle evenementielle
*/
     viewerFigures viewer(argc, argv);
     viewer.run();

     return 0;
/*
    vector <FigureGeometrique*> figures{
        new Ligne({1,0,0}, {0,0}, {100,200}),
        new PolygoneRegulier({0,1,0}, {100,200}, 50, 5)
    };

    for(FigureGeometrique * f : figures)
        f->afficher();

    for(FigureGeometrique * f : figures)
        delete f;
    return 0;
    */
}

