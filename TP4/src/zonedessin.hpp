#ifndef ZONEDESSIN_HPP
#define ZONEDESSIN_HPP
#include"figuregeometrique.hpp"
#include <gtkmm.h>
#include<random>

class zoneDessin : public Gtk::DrawingArea
{
private:
    std::mt19937 _engine;
    std::uniform_int_distribution<int> _distRayon;
    std::uniform_int_distribution<int> _distCotes;
    std::vector<FigureGeometrique*> _figures;
public:
    zoneDessin();
    ~zoneDessin();
    bool on_draw(const Cairo::RefPtr<Cairo::Context> &) override;
    bool gererClic(GdkEventButton* event);
};

#endif // ZONEDESSIN_HPP
