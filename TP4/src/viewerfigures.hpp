#ifndef VIEWERFIGURES_HPP
#define VIEWERFIGURES_HPP

#include <gtkmm.h>
#include"zonedessin.hpp"

class viewerFigures
{
private:
    Gtk::Main _kit;
    Gtk::Window _window;
    zoneDessin _dessin;
public:
    viewerFigures(int argc, char **argv);
    void run();
};

#endif // VIEWERFIGURES_HPP
