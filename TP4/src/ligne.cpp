#include "ligne.hpp"
#include <iostream>
#include<gtkmm.h>

using namespace std;

Ligne::Ligne(const Couleur & couleur, const Point & p0, const Point & p1):
    FigureGeometrique(couleur), _p0(p0), _p1(p1){}


void Ligne::afficher(const Cairo::RefPtr<Cairo::Context> & context) const{
    cout << "Ligne " << getCouleur()._r << "_"<<getCouleur()._g<< "_"<< getCouleur()._b << " "
         << getP0()._x << "_" << getP0()._y << " "
         << getP1()._x << "_" << getP1()._y << endl;

    const Couleur & c = getCouleur();
    context->set_source_rgb(c._r, c._g, c._b);

    context->move_to(_p0._x, _p0._y);
    context->line_to(_p1._x, _p1._y);

    context->stroke();
}

const Point & Ligne::getP0() const{
    return _p0;
}
const Point & Ligne::getP1() const{
    return _p1;
}
