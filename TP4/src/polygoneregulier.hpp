#ifndef POLYGONEREGULIER_HPP
#define POLYGONEREGULIER_HPP
#include "couleur.hpp"
#include "figuregeometrique.hpp"
#include "ligne.hpp"
#include "point.hpp"
#include <iostream>

using namespace std;

class PolygoneRegulier: public FigureGeometrique
{
private:
    int _nbPoints;
    Point * _points;
public:
    PolygoneRegulier(const Couleur & couleur, const Point & center, int rayon, int nbCotes);
    void afficher(const Cairo::RefPtr<Cairo::Context> & context) const override;
    int getNbPoints() const;
    const Point & getPoint(int indice) const;
    ~PolygoneRegulier();
};

#endif // POLYGONEREGULIER_HPP
