#define _USE_MATH_DEFINES
#include "polygoneregulier.hpp"
#include <cmath>


PolygoneRegulier::PolygoneRegulier(const Couleur & couleur, const Point & center, int rayon, int nbCotes):
    FigureGeometrique(couleur), _nbPoints(nbCotes)
{
   _points = new Point[_nbPoints];
    for(int i=0; i<_nbPoints; i++){
        double angle = 2*M_PI*i/double(_nbPoints);
        int x = rayon * cos(angle) + center._x;
        int y = rayon * sin(angle) + center._y;
        _points[i] = {x,y};
    }
}

void PolygoneRegulier::afficher(const Cairo::RefPtr<Cairo::Context> & context) const{
    const Couleur & c = getCouleur();
    context->set_source_rgb(c._r, c._g, c._b);

    const Point &p1 = _points[_nbPoints-1];
    context->move_to(p1._x, p1._y);

    for(int i = 0; i<_nbPoints; i++){
        const Point & p = _points[i];
        context->line_to(p._x, p._y);
    }

    context->stroke();
}

int PolygoneRegulier::getNbPoints() const{
    return _nbPoints;
}

const Point & PolygoneRegulier::getPoint(int indice) const{
    return _points[indice];
}


PolygoneRegulier::~PolygoneRegulier(){
 delete [] _points;
}
