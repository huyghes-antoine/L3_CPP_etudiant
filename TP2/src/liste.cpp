#include "liste.hpp"

liste::liste():
    _tete(nullptr){
}

void liste::ajouterDevant(int valeur){
    _tete = new Noeud{ valeur, _tete};
}

int liste::getTaille() const {
    if(_tete == nullptr)
        return 0;
    int cpt = 0;
    Noeud* tmp = _tete;
    while(true){
        cpt++;
        if ( tmp->_suivant == nullptr)
            return cpt;
        tmp = tmp->_suivant;
    }
}

int liste::getElement(int indice) const {
    if( _tete == nullptr || indice >= getTaille() || indice < 0)
           return -666;
    Noeud* tmp = _tete;

    for(int i = 0; i<indice ; i++)
        tmp = tmp->_suivant;
    return tmp->_valeur;
}
