#ifndef LISTE_HPP
#define LISTE_HPP

struct Noeud{
  int _valeur;
  Noeud* _suivant;
};

struct liste{
   Noeud* _tete;
   liste();
   void ajouterDevant(int valeur);
   int getTaille() const;
   int getElement(int indice) const ;
};
#endif // LISTE_HPP
