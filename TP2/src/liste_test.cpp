#include "liste.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupListe) { };

TEST(GroupListe, liste_vide)  {
    liste l;
    CHECK_EQUAL(0, l._tete);
}

TEST(GroupListe, liste_ajoutDevant){
    liste l;
    l.ajouterDevant(2);
    CHECK_EQUAL(2, l._tete->_valeur);
}

TEST(GroupListe, liste_gettaille){
    liste l;
    l.ajouterDevant(10);
    CHECK_EQUAL(1, l.getTaille());
}

TEST(GroupListe, liste_getElement){
    liste l;
    l.ajouterDevant(1);
    l.ajouterDevant(99);
    l.ajouterDevant(10);
    CHECK_EQUAL(1, l.getElement(2));
}
