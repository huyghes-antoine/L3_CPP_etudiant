#include "Doubler.hpp"
#include "liste.hpp"

#include <iostream>

int main() {
    //std::cout << doubler(21) << std::endl;
    //std::cout << doubler(21.f) << std::endl;

/*
    //SCENARIO A
    int a = 42;
    int* p = &a;
    std::cout << a << std::endl;
    *p = 37;
    std::cout<< a << std::endl;

    //SCENARIO B
    int *t = new int[10];
    t[2] = 42;
    std::cout << "t[2] :" << t[2] << std::endl;
    delete[]t;
    std::cout << "t[2] :" << t[2] << std::endl;
     t =nullptr;
*/

    liste* l = new liste;
    l->ajouterDevant(13);
    l->ajouterDevant(37);
    l->ajouterDevant(34);
    for(int i = 0; i < l->getTaille(); i++)
        std::cout << l->getElement(i) << std::endl;
    return 0;
}

